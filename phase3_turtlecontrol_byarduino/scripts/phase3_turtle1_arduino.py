#!/usr/bin/env python

##This program made by Bittu scaria of middlesex UNiversity dubai.
##Special thanks to Professor Usman Mohammed Middlesex Dubai for his support and guidance in making this program executable to project
##this program is made to work with Turtlebot1 driven by Arduino Publisher.
##follow readmeinstructions attached with this project folder for more details.


import rospy
from geometry_msgs.msg import Twist
##obove commands importing ros related environment and libraray for python
##message type used is geometry typoe and Twist is the message name.



def callback(data, velocity_publisher):		###defining the call back function in the bracket we have data and velocity_publisher which helps the subricber to point to teh call back function
	rospy.loginfo(data.linear.x)			##displaying logging infromation of node and what data is subscribed at data.linear.x
	vel_msg = Twist()				##assigning Twist message as vel_msg
	vel_msg.linear.x =  data.linear.x/2.0   ###we are only publishing half of the subribed value from Arduino topic named cmd_vel to turtle1/cmd_vel
	x_value = data.linear.x   ## we need to take only x value of the whole twist message
	velocity_publisher.publish(vel_msg)		###we are publishing vel_msg after computing it inside this python program
	print(vel_msg.linear.x)			###printing the vel_msg.linear.x value at screen so that we can see teh processed value after computing

def move():					###defining move function
    # Starts a new node
    rospy.init_node('movebyarduino', anonymous=True)		##initiating node make sure your nod ename is writen same as python program name

    velocity_publisher = rospy.Publisher('/turtle1/cmd_vel', Twist, queue_size=10) 		##publisher node definition
    rospy.Subscriber('/cmd_vel' ,Twist , callback, velocity_publisher)			##defining subscriber and poiting to call back with argument velocity_publisher
 
    
    while not rospy.is_shutdown():
	driving_forward = True
	

if __name__ == '__main__':
    try:
        #Testing our function
        move()
    except rospy.ROSInterruptException: pass

rate.sleep()  ###repeats program same as rospin()


