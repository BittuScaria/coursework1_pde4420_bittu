/*
 * this program is written by bittu scaria middlesex university dubai and has used http://wiki.ros.org/rosserial_arduino/Tutorials as reference
 * i am trying to communicate with rosserial  and this ia a Publisher Example
 * Prints "hello Robot System!" on the screen and used with 'Listentoarduino.py' subscriber program 
 * this program is refered to the arduinorosserial tutorials Hello World example.
 */



#include <ros.h>                              ////standard command to include ros livbary into arduino
#include <std_msgs/String.h>                   ////we are defining the type of message as standard and in string format 
                                               ///with out this two cammanda Arduino will not be able to locate this files

ros::NodeHandle  nh;                          ///which allows program to create publishers and subscribers,in this case a publisher
                                                ///this also take care of serial communication with ROS

std_msgs::String str_msg;                     ////defines message 
ros::Publisher chatter("chatter", &str_msg);    ////publishing a topic named chatter

char hello[32] = "hello Robot System!";     ////defining what to be included in cahtter message or dtaa what chatter topic should carry

void setup()
{
  nh.initNode();                              ////intiating the node to communicate
  nh.advertise(chatter);                    //////this function helps to publish the chatter topic the system
}

void loop()
{
  str_msg.data = hello;
  chatter.publish( &str_msg );
  nh.spinOnce();                             ////this functions hnadles all callbacks related to ROS communication
  delay(1000);
}
