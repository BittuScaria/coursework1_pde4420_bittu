#!/usr/bin/env python

# program created by Bittu SCaria Middlesex University,Dubai to test rosserial communication,
# run the HelloROS arduino program at Ardduino IDE ,
# Then run roscore on Terminal 1;
# Terminal 2 run rosserial command
# Exectute this node via rosrun on terminal 3
# incase of any doubts please contact me on robotbits007@gmail.com


import rospy
from std_msgs.msg import String

def callback(data):
    rospy.loginfo(rospy.get_caller_id() + 'I heard from Arduino %s', data.data)

def listener():

    # In ROS, nodes are uniquely named. If two nodes with the same
    # name are launched, the previous one is kicked off. The
    # anonymous=True flag means that rospy will choose a unique
    # name for our 'listener' node so that multiple listeners can
    # run simultaneously.
    rospy.init_node('Listentoarduino', anonymous=True)

    rospy.Subscriber('chatter', String, callback)

    # spin() simply keeps python from exiting until this node is stopped
    rospy.spin()

if __name__ == '__main__':
    listener()
