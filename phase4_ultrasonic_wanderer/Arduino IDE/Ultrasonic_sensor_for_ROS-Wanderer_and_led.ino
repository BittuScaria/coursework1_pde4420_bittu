



#include <ros.h>
#include <ros/time.h>
#include <std_msgs/Empty.h>
#include <sensor_msgs/Range.h>


ros::NodeHandle  nh;

void messageCb( const std_msgs::Empty& toggle_msg){
  digitalWrite(03, HIGH-digitalRead(03));   // blink the led
}

ros::Subscriber<std_msgs::Empty> sub("toggle_led", messageCb );

sensor_msgs::Range range_msg;
ros::Publisher pub_range( "/ultrasound", &range_msg);




char frameid[] = "/ultrasound";

const unsigned int TRIG_PIN=13;
const unsigned int ECHO_PIN=12;
const unsigned int BAUD_RATE=57600;

void setup() {
  pinMode(TRIG_PIN, OUTPUT);
  pinMode(ECHO_PIN, INPUT);
  Serial.begin(57600);


nh.initNode();
nh.advertise(pub_range);

  range_msg.radiation_type = sensor_msgs::Range::ULTRASOUND;
  range_msg.header.frame_id =  frameid;
  range_msg.field_of_view = 0.1;  // fake
  range_msg.min_range = 0.0;
  range_msg.max_range = 6.47;
  
  pinMode(8,OUTPUT);
  digitalWrite(8, LOW);

pinMode(03, OUTPUT);
  nh.initNode();
  nh.subscribe(sub);


  
}

long range_time;

void loop() {
  digitalWrite(TRIG_PIN, LOW);
  delayMicroseconds(2);
  digitalWrite(TRIG_PIN, HIGH);
  delayMicroseconds(10);
  digitalWrite(TRIG_PIN, LOW);
  
  

 const unsigned long duration= pulseIn(ECHO_PIN, HIGH);
 int ultrasound = duration/29/2;
 if(duration==0){
   //Serial.println("Warning: no pulse from sensor");
   


  
    range_time =  millis() + 200;
  }
  else{

    range_msg.range = ultrasound;
    pub_range.publish(&range_msg);
    range_time =  millis() + 200;

    
      //Serial.print("distance to nearest object:");
      Serial.println(ultrasound);
      
      //Serial.println(" cm");
      delay(200);

  } 

 nh.spinOnce();
 delay(200);
 }
