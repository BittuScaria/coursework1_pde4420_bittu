Project details

This project is made to test an ULTRASONIC(SONIC) SENSOR with ARduino publishing the range to ROS and by a ROS python node we subscribe the data and based on the sensor input ,we make the turtle to react on virtual/simulation world.

Refernce documents and webistes as follows;
http://wiki.ros.org/
http://wiki.ros.org/rosserial_arduino/Tutorials
Programming Robots with ROS by Morgan Quigley, Brian Gerkey,and William D. Smart



Hardware used:
arduino mega 2560
ultrasonic sensor HY-SRF05
USB Cable
male to male cable-4nos
Blue LED with resistor and two jumbler cable for LED VERSION

Hardware setup:mentioned in attach document "setup documentation"


to test if the ULtrasonic sensor is giving the right vale we have a testing node also build for the project;
----------------------------------------------------------------
Testing phase
run roscore on Terminal 1------starting the rose engine
open Arduino IDE ,,open ,verify and Uplaod Ultrasonic_sensor_for_ROS-Wanderer program under Arduino IDE folder
Terminal 2 run  $ rosrun rosserial_python serial_node.py /dev/ttyACM0 ------------to start the rosserial communication with arduino
On terminal 3 $rosrun phase4_ultrasonic_wanderer ultrasonic_ahead.py

by running this we can see the actual values of ULtrasonic sensor getting displayed on screen.
if reacting to your hand placed infront of sensore and moving close and away from sensor should changes values accordingly.

---------------------------------------------------------------------
Running Phase 4 project
run roscore on Terminal 1------starting the rose engine
open Arduino IDE ,,open ,verify and Uplaod Ultrasonic_sensor_for_ROS-Wanderer program under Arduino IDE folder
Terminal 2 run  $ rosrun rosserial_python serial_node.py /dev/ttyACM0 ------------to start the rosserial communication with arduino
terminal 3 run roslaunch turtlebot_gazebo turtlebot_world.launch
On terminal 4 $ rosrun ultrasonic_sacner ultrasonic_wand.py cmd_vel:=cmd_vel_mux/input/teleop

we can see the turtle moves straight then rotates and again moves traight if no objects are detected in front of the ultrasonic sensor in real world.If you place your hand less than 10cm distance turtle will keep on rotating untill you move your hand out of the sensor.And after detecting the object turtle changes its direction of movement.

-----------------------------


Running Phase 4 project with LED
run roscore on Terminal 1------starting the rose engine
open Arduino IDE ,,open ,verify and Uplaod Ultrasonic_sensor_for_ROS-Wanderer LED program under Arduino IDE folder
Terminal 2 run  $ rosrun rosserial_python serial_node.py /dev/ttyACM0 ------------to start the rosserial communication with arduino
terminal 3 run roslaunch turtlebot_gazebo turtlebot_world.launch
On terminal 4 $ rosrun ultrasonic_sacner wandere_LED.py cmd_vel:=cmd_vel_mux/input/teleop

we can see the turtle moves straight then rotates and again moves traight if no objects are detected in front of the ultrasonic sensor in real world.If you place your hand less than 10cm distance turtle will keep on rotating untill you move your hand out of the sensor.And after detecting the object turtle changes its direction of movement.while moving straight it blinks blue led on the breadboard circuit basically toggles each time the action between off and on.





