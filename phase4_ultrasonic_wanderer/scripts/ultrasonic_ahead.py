#!/usr/bin/env python

###with this progam we can get the value of ultrasonic sensor printed on the screen 
##after running arduino IDE program and executing rosserial command followed by this python node.
###thanks LORD for making me to understand and make it to work.
#Testing phase
#run roscore on Terminal 1------starting the rose engine
#open Arduino IDE ,,open ,verify and Uplaod Ultrasonic_sensor_for_ROS-Wanderer program under Arduino IDE folder
#Terminal 2 run  $ rosrun rosserial_python serial_node.py /dev/ttyACM0 ------------to start the rosserial communication with arduino
#On terminal 3 $rosrun phase4_ultrasonic_wanderer ultrasonic_ahead.py

#by running this we can see the actual values of ULtrasonic sensor getting displayed on screen.
#if reacting to your hand placed infront of sensore and moving close and away from sensor should changes values accordingly.

#Refernce documents and webistes as follows;
#http://wiki.ros.org/
#http://wiki.ros.org/rosserial_arduino/Tutorials
#Programming Robots with ROS by Morgan Quigley, Brian Gerkey,and William D. Smart




import rospy			#importing ros python library
from sensor_msgs.msg import Range	#from sesnor message type of message get Range

###defining the call back function a sscan_callback
def scan_callback(msg):
	range_ahead = msg.range	#defining what is range_ahead to get message from message.range
	print "ultrasonic_ahead: %0.1f" % range_ahead		###printing on screen
rospy.init_node('ultrasonic_ahead')		#initiate node
scan_sub = rospy.Subscriber('ultrasound', Range, scan_callback)			##defining ros subscriber
rospy.spin()
