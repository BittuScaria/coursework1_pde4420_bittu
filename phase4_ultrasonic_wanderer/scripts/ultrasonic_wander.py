#!/usr/bin/env python


#Program details

#This project is made to test an ULTRASONIC(SONIC) SENSOR with ARduino publishing the range to ROS and by a ROS python node we subscribe the data and based on the sensor input ,we make the turtle to react on virtual/simulation world.
#Running Phase 4 project
#run roscore on Terminal 1------starting the rose engine
#open Arduino IDE ,,open ,verify and Uplaod Ultrasonic_sensor_for_ROS-Wanderer program under Arduino IDE folder
#Terminal 2 run  $ rosrun rosserial_python serial_node.py /dev/ttyACM0 ------------to start the rosserial communication with arduino
#terminal 3 run roslaunch turtlebot_gazebo turtlebot_world.launch
#On terminal 4 $ rosrun ultrasonic_sacner ultrasonic_wand.py cmd_vel:=cmd_vel_mux/input/teleop
#we can see the turtle moves straight then rotates and again moves traight if no objects are detected in front of the ultrasonic sensor in real world.If you place your hand less than 10cm distance turtle will keep on rotating untill you move your hand out of the sensor.And after detecting the object turtle changes its direction of movement.



#Refernce documents and webistes as follows;
#http://wiki.ros.org/
#http://wiki.ros.org/rosserial_arduino/Tutorials
#Programming Robots with ROS by Morgan Quigley, Brian Gerkey,and William D. Smart


import rospy
from geometry_msgs.msg import Twist
from sensor_msgs.msg import Range

def scan_callback(msg):
	global sonar_head
	sonar_head = msg.range
sonar_head = 1 # anything to star

scan_sub = rospy.Subscriber('ultrasound', Range, scan_callback)
cmd_vel_pub = rospy.Publisher('cmd_vel', Twist, queue_size=1)
rospy.init_node('ultrasonic_wander')
state_change_time = rospy.Time.now()
driving_forward = True
rate = rospy.Rate(10)
while not rospy.is_shutdown():
	if driving_forward:
		if (sonar_head < 10.0 or rospy.Time.now() > state_change_time):
			driving_forward = False
			state_change_time = rospy.Time.now() + rospy.Duration(5)
	else: # we're not driving_forward
		if rospy.Time.now() > state_change_time:
			driving_forward = True # we're done spinning, time to go forward!
			state_change_time = rospy.Time.now() + rospy.Duration(10)
		twist = Twist()
		if driving_forward:
			twist.linear.x = 1
		else:
			twist.angular.z = 1
		cmd_vel_pub.publish(twist)
		rate.sleep()
