Fri 02 Nov 2018 11:57:33 AM +04 

Refernce:
Project is based on the tutorials you find on following link http://www.clearpathrobotics.com/assets/guides/ros/Driving%20Husky%20with%20ROSSerial.html

Husky Drive Arduino V1 program is biuld to test an input from Arduino publisher under the topic "cmd/vel"
This topic is been used as subscriber by the HUSKY simulator to move husky robot in the simulation world.



pre reqiuirements

HUsky simulator ,if not installed follow below mentioned steps
open a new terminal,enter as follows;
sudo apt-get update
sudo apt-get install ros-indigo-husky-desktop
sudo apt-get install ros-indigo-husky-simulator



Hardware used; 
	Arduino type:Arduino UNO
	USB cable
	jumber cable

Hardware  setup as folows :
connect arduino hardware as shown in the picture in enclosed document "setup reference document".

Execution Procedure:
launch Arduino IDE and open program named Husky_Arduino_v1.ino available in the phase_1 /Huskydrive_Arduino_v1 folder.
verify and upload program to Arduino.
Open terminal 1 and run roscore
On terminal 2  	roslaunch husky_gazebo husky_empty_world.launch
On Terminal 3	rosrun rosserial_python serial_node.py /dev/ttyACM0 

Now connect jumber cable from 5v to pin 4 for moving HUSKY forward and pin 8 to move backward.


Refernce:
Project is based on the tutorials you find on following link http://www.clearpathrobotics.com/assets/guides/ros/Driving%20Husky%20with%20ROSSerial.html 
