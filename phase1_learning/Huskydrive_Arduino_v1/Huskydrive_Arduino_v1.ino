
/*program is made by bittu scaria Middlesex Univeristy,Dubai
 * it is made in refernce to http://www.clearpathrobotics.com/assets/guides/ros/
 * this program has been modified and used for the current project
 * execution instruction are available in readme instruction on the folder
 */







#include <ArduinoHardware.h>                //// /standard command to include  arduino hardware
#include <ros.h>                       //// /standard command to include ros livbary into arduino
#include <geometry_msgs/Twist.h>      /////geomtery_msgs is the type of message and twist is the message name what we are using in this program

ros::NodeHandle nh;                 //which allows program to create publishers and subscribers,

geometry_msgs::Twist msg;         ////defines message

ros::Publisher pub("/cmd_vel",&msg);  /////publishing a topic named /cmd_vel




void setup()
{
 nh.initNode();
 nh.advertise(pub);

} 


void loop()
{
 if(digitalRead(8)==1)
 msg.linear.x=-0.75;

///////we are publishing the x linear of twist message as -0.75 when didgital input8 of arduino is high

else if (digitalRead(4)==1)
msg.linear.x=0.75;

///////we are publishing the x linear of twist message as +0.75 when didgital input4 of arduino is high

else if (digitalRead(8)==0 && digitalRead(4)==0)
msg.linear.x=0;

/////program will be publishing x linear of twist message as 0.0 when eithe rof inputs are low.

pub.publish(&msg);
nh.spinOnce();
}
