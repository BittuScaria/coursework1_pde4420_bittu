

#include <ros.h>
#include <ros/time.h>
#include <std_msgs/Float32.h>


std_msgs::Float32 dist_msg;
ros::Publisher pub_dist( "distance", &dist_msg);
ros::NodeHandle  nh;



const unsigned int TRIG_PIN=13;
const unsigned int ECHO_PIN=12;
const unsigned int BAUD_RATE=57600;

void setup() {
  pinMode(TRIG_PIN, OUTPUT);
  pinMode(ECHO_PIN, INPUT);
  Serial.begin(57600);


nh.initNode();
nh.advertise(pub_dist);
   
  
}

long dist_time;

void loop() {
  digitalWrite(TRIG_PIN, LOW);
  delayMicroseconds(2);
  digitalWrite(TRIG_PIN, HIGH);
  delayMicroseconds(10);
  digitalWrite(TRIG_PIN, LOW);
  
  

 const unsigned long duration= pulseIn(ECHO_PIN, HIGH);
 int distance = duration/29/2;
 if(duration==0){
   //Serial.println("Warning: no pulse from sensor");
   


  
    dist_time =  millis() + 200;
  }
  else{

    dist_msg.data = distance;
    pub_dist.publish(&dist_msg);
    dist_time =  millis() + 200;

    
      //Serial.print("distance to nearest object:");
      Serial.println(distance);
      
      //Serial.println(" cm");
      delay(200);

  } 

 nh.spinOnce();
 delay(200);
 }
