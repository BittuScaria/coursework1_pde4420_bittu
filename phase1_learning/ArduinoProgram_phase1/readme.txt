I am trying to work out with an ultrasonic sensor creating a publisher and within ROS i am able to get the data from the sensor in a meaning full manner.


SENSOR USED:HY-SRF05 
TYPE:ULTRASONIC SENSOR
arduino UNO micro controller is used with dev/tty/ACM0 communication protocol.

PIN CONFIGURATION OF THE SYSTEM;

vcc-5v on the arduino 
Trig-pin number 13 on arduino
Echo-pin number 12 on arduino
GNC-Ground on arduino



After hardware conections open Arduino IDE and load the program under folder ArduinoProgram_phase1.
verify and upload it.

First terminal---run roscore
second terminal----
		$ sudo chmod a+rw /dev/ttyACM0

		$ rosrun rosserial_python serial_node.py /dev/ttyACM0

third terminal----rostopic list

			we can see all teh activbe list of topics including distance what we are 				publishing the data in this project
		

		rostopic echo distance----we can now see the value from ultrasonic sensor egetting 			updated in the third terminal

